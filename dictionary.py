fh = open("access.log")

keywords = "Windows", "Linux", "Mac", "Android", "facebookexternalhit" #add android, yandex and many other platforms from the log sheet
#d = {"Windows":0, "Linux":0, "Mac":0}
d = {}
total = 0 #Create integer variable

#Exercises for practising at home?
# * Most visited URL?
# * Most used user agent?
# * Most used OS?

for line in fh:
	total = total + 1
	try:
		source_timestamp, request, reponse, _, _, agent, _ = line.split("\"")
		method, path, protocl = request.split(" ")
		for keyword in keywords:
			if keyword in agent:
				d[keyword] = d.get(keyword, 0) + 1
				try:
					d[keyword] = d[keyword] + 1
				except KeyError:
					d[keyword] = 1
				break #this will stop searching other keywords
	
	except ValueError:
		pass #This will do nothing, needed due to syntax or skips the counting
	
interesting_total = sum(d.values())
print "Total requests", total
print d


print "Total lines with requested keywords", sum(d.values())

l = d.items()
l = sorted(l, key = lambda t:t[1], reverse=True) #Use this for python 3
l.sort(key = lambda (keyword,hits):-hits) #//WARNING: will not work in python3
for key, value in l:

	print key, "==>", value, "(", value * 100 / interesting_total, "%)" #similar option below
	#print "%s => %d (%.02f%%)" % (key, value, value * 100 / total)
	# %d (%.02f%%) --> %d = requesting decimal points ; 02 = decimal points before comma %% = percentage sign
